package grid;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SeleniumGrid {

	/** Download Selenium Grid {@link https://bit.ly/2TlkRyu} */

	/**
	 * Paste the downloaded File on the Desired Folder example
	 * D:\SeleniumGrid\selenium-server-standalone-3.141.59.jar
	 */

	/** Starting Hub with the below command */

	/**
	 * 
	 * <ul>
	 * <li>command</li>
	 * <li>java -jar selenium-server-standalone.jar -role hub</li>
	 * <li>- Nodes should register to http://192.168.0.182:4444/grid/register/</li>
	 * <li>- Clients should connect to http://192.168.0.182:4444/wd/hub</li>
	 * </ul>
	 * 
	 */

	/** Registering Node with the Hub using below Command */

	/**
	 * 
	 * <ul>
	 * <li>command</li>
	 * <li>java -Dwebdriver.chrome.driver=chromedriver.exe -jar
	 * selenium-server-standalone.jar -role node -nodeConfig node1Config.json</li>
	 * </ul>
	 * 
	 * @throws MalformedURLException
	 * 
	 */
	

	RemoteWebDriver driver;

	String clientUrl;

	/*
	 * @BeforeMethod public void prereq() throws MalformedURLException {
	 * 
	 * String clientUrl = "http://192.168.0.182:4444/wd/hub";
	 * 
	 * DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	 * 
	 * driver = new RemoteWebDriver(new URL(clientUrl), capabilities);
	 * 
	 * driver.manage().window().maximize();
	 * 
	 * }
	 */

	@BeforeTest
	@Parameters({ "browserName" })
	public void setUp(String browserName) throws MalformedURLException {

		clientUrl="http://192.168.0.182:4444/wd/hub";
		
		if (browserName.equals("chrome")) {

			DesiredCapabilities capabilities = DesiredCapabilities.chrome();

			driver = new RemoteWebDriver(new URL(clientUrl), capabilities);

			driver.manage().window().maximize();

		} else if (browserName.equals("firefox")) {

			DesiredCapabilities capabilities = DesiredCapabilities.firefox();

			driver = new RemoteWebDriver(new URL(clientUrl), capabilities);

			driver.manage().window().maximize();

		}

	}
	
	@AfterTest
	public void kill() {
		
		driver.quit();
	}

}
