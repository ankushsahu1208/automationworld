package org.jan31;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class TestBase {

	WebDriver driver;

	@BeforeTest
	public void setup() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\ankushs\\git\\automationworld\\blog\\Drivers\\chromedriver.exe");

		driver = new ChromeDriver();
		
		Dimension d= new Dimension(1920, 2000);

		driver.manage().deleteAllCookies();
		driver.manage().window().setSize(d);
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@AfterMethod
	public void getScreenShotAfterMethod() throws IOException {

		TakesScreenshot ts = (TakesScreenshot) driver;
		File image = ts.getScreenshotAs(OutputType.FILE);
		File file = new File(
				"C:\\Users\\ankushs\\git\\automationworld\\blog\\ScreenShots\\" + getCalendarXpath() + ".png");
		FileUtils.copyFile(image, file);
	}

	@AfterTest
	public void quit() {

		driver.quit();
	}

	/** This method will return the Calendar Xpath */

	public String getCalendarXpath() {

		/** formatting Date : 2020_01_31_AD_19_44_38 */
		SimpleDateFormat format = new SimpleDateFormat();
		format.applyPattern("yyyy_MM_dd_G_HH_mm_ss");

		/** Creating Date class Object */
		Date date = new Date();

		/** Setting Date */
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);

		/** Getting Today's date */
		Date instantDate = cal.getTime();
		System.out.println(instantDate);
		System.err.println(format.format(instantDate));

		return format.format(instantDate);

	}

	// -------------------------------------------------------------------------------------------------------------------------------------------------

}
