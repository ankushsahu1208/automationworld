package org.jan31;

import java.awt.AWTException;
import java.awt.Button;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class RegressionTestCases extends TestBase {

	@Test(priority = 1)
	public void verifyTitle() throws AWTException, InterruptedException {

		driver.get("https://unifytest1.webgility.com/");

		System.err.println("Zoom");

		Robot robot = new Robot();

		robot.keyPress(KeyEvent.VK_F10);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(500);

		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(500);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(500);

		robot.keyPress(KeyEvent.VK_F10);
		Thread.sleep(500);

		Thread.sleep(5000);

		robot.keyRelease(KeyEvent.VK_TAB);

		robot.keyRelease(KeyEvent.VK_F10);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_TAB);
		Thread.sleep(5000);

		System.err.println("Zoom  end");

	}

	@Test(priority = 2, enabled = true)
	public void zoomOut() throws AWTException, InterruptedException {

		driver.findElement(By.name("username")).sendKeys("Ankuis");

		Thread.sleep(5000);

		driver.findElement(By.name("password")).sendKeys("Ankuis");

		Thread.sleep(5000);

		WebElement btn=driver.findElement(By.xpath("//button[@id='button']"));
		
		JavascriptExecutor jse= (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click();", btn);

		Thread.sleep(50000);
	}

}
