package javaProgram;

public class ReverseString {

	public static void main(String[] args) {

		String originalString = "madam";

		StringBuffer strBfr1 = new StringBuffer(originalString);

		StringBuffer reverse = strBfr1.reverse();

		String reversedString = new String(reverse);

		if (originalString.equalsIgnoreCase(reversedString)) {

			System.out.println("Original and Reversed String are the same ");
			System.out.println("String is Palindrome");
		} else {

			System.out.println("Original and Reversed String are different");
			System.out.println("String is not Palindrome");
		}

	}

}
