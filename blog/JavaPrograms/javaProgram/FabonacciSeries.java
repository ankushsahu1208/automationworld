package javaProgram;

import java.util.Scanner;
/** 
 * 
 * Fabonacci Series Program in Java
 *
 */

public class FabonacciSeries {

	public static void main(String[] args) {

		int s1 = 0, s2 = 1;

		System.out.print("Enter the number ");

		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();

		for (int i = 1; i <= number; ++i) {

			System.out.println(s1);

			int sum = s1 + s2;

			s1 = s2;

			s2 = sum;

		}
	}

}
